#include <iostream>
using namespace std;

const int MAX = 1000;
const int INF = 1000000001;

int n, m;
char a[MAX][MAX];
int dist[MAX][MAX];
int dir[4][2] = { {-1, 0}, {0, 1}, {1, 0}, {0, -1} };

void bfs(int row, int col) {
    memset(dist, 63, sizeof(dist));
    queue < pair <int, int> > q;
    q.push(make_pair(row, col));
    dist[row][col] = 0;
    while (!q.empty()) {
        row = q.front().first;
        col = q.front().second;
        q.pop();
        for (int i = 0; i < 4; i++) {
            int nrow = row + dir[i][0]; if (nrow < 0 || nrow >= n) continue;
            int ncol = col + dir[i][1]; if (ncol < 0 || ncol >= m) continue;
            if (a[nrow][ncol] == 'T')
                continue;
            if (dist[row][col] + 1 < dist[nrow][ncol]) {
                dist[nrow][ncol] = dist[row][col] + 1;
                q.push(make_pair(nrow, ncol));
            }
        }
    }
}


int main() {
	cin >> n >> m;
	for (int i = 0; i < n; i++)
	    scanf("%s", a[i]);

	int srow = -1, scol = -1;
	for (int i = 0; i < n; i++) {
	    for (int c = 0; c < m; c++) {
	        if (a[i][c] == 'E') {
	            srow = i;
	            scol = c;
	            a[i][c] = '0';
	            break;
	        }
	    }
	    if (srow != -1 || scol != -1)
	        break;
	}
	bfs(srow, scol);

	int len = INF;
	for (int i = 0; i < n; i++)
	    for (int c = 0; c < m; c++)
	        if (a[i][c] == 'S') len = dist[i][c], a[i][c] = '0';

	int ans = 0;
	for (int i = 0; i < n; i++) {
	    for (int c = 0; c < m; c++) {
	        if (isdigit(a[i][c]) && dist[i][c] <= len)
	            ans += a[i][c] - '0';
	    }
	}
	printf("%d\n", ans);
	return 0;
}
