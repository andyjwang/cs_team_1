// CodeForces Round 65, Problem C.
#include <iostream>
#include <vector>

using namespace std;

// larger than the upper limit
const int N = 1000043;
vector<int> adjList[N];

// record of all nodes that have been seen before, and counted
int seenBefore[N];
// array of size of total network starting from each index, saved at that index in result
int result[N];

int n, m;
int idx = 0;

// Edited DFS
int dfs(int x)
{
    // Break
    if(seenBefore[x])
        return 0;
    seenBefore[x] = idx;

    int ans;
    if (x < n){
        ans = 1;
    }
    else{
        ans = 0;
    }


    // Go through each neighboring node after exploring each node all the way through
    for(int i = 0; i < adjList[x].size(); i ++)
        ans += dfs(adjList[x][i]);

    // return amount of nodes
    return ans;
}

int main()
{
    // std input
    cin >> n >> m;
    for(int i = 0; i < m; i++)
    {
        int k;
        cin >> k;

        for(int j = 0; j < k; j++)
        {
            int x;
            cin >> x;
            x--;

            adjList[x].push_back(i + n);
            adjList[i + n].push_back(x);
        }
    }


    // go through every starting node and run DFS
    for(int i = 0; i < n; i++)
    {
        // ok to skip seenBefore nodes because they have the same network size as every node in their network
        if(!seenBefore[i])
        {
            idx++;
            result[idx] = dfs(i);
        }
        // std output
        printf("%d ", result[seenBefore[i]]);
    }
}